import numpy as np
from sokudo import Benchmark, State
from sokudo.reporters import ConsoleReporter, JsonReporter, BoxplotReporter

bench = Benchmark("Norm", warmup=1)
bench.unit = "us"
n_iterations = 10_000
n_repeat = 7
args = {"n": 10**5}
rng = np.random.default_rng()


@bench.register(iterations=n_iterations, repetitions=n_repeat, args=args)
def np_norm(state: State, n: int) -> None:
    x = rng.normal(size=n)

    for _ in state:
        np.linalg.norm(x)


@bench.register(iterations=n_iterations, repetitions=n_repeat, args=args)
def np_sum_sq(state: State, n: int) -> None:
    x = rng.normal(size=n)

    for _ in state:
        np.sqrt(np.sum(x**2))


if __name__ == "__main__":
    bench.register_reporter(ConsoleReporter())
    bench.register_reporter(JsonReporter("output.json"))
    bench.register_reporter(BoxplotReporter("output.jpg"))
    bench.run()
