import numpy as np
import math
from sokudo import Benchmark, State
from sokudo.reporters import ConsoleReporter

bench = Benchmark("Log")
bench.unit = "ms"
n_repeat = 5
args = {"n": [10**i for i in range(3, 7)]}
rng = np.random.default_rng()


@bench.register(repetitions=n_repeat, args=args)
def math_log(state: State, n: int) -> None:
    x = rng.integers(1, 10**4, size=n)
    y = [0.0] * n

    for _ in state:
        for i in range(n):
            y[i] = math.log(x[i])


@bench.register(repetitions=n_repeat, args=args)
def np_log(state: State, n: int) -> None:
    x = rng.integers(1, 10**4, size=n)

    for _ in state:
        np.log(x)


if __name__ == "__main__":
    bench.register_reporter(ConsoleReporter())
    bench.run()
