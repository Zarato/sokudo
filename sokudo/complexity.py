import numpy as np
from typing import Callable, List

from sokudo.types import ArrayLike

ElementWiseFunc = Callable[[ArrayLike], ArrayLike]


class Complexity:
    def __init__(self, name: str, func: ElementWiseFunc):
        """Time complexity.

        :param name: Name.
        :type name: str
        :param func: Running time.
        :type func: ElementWiseFunc
        """
        self._name = name
        self._func = func

    @property
    def name(self) -> str:
        """Complexity's name.

        :return: Name.
        :rtype: str
        """
        return self._name

    @property
    def func(self) -> ElementWiseFunc:
        """Time complexity.

        :return: Function that computes time complexity.
        :rtype: ElementWiseFunc
        """
        return self._func

    def __call__(self, *args, **kwargs) -> ArrayLike:
        """Compute time complexity.

        :return: Time complexity.
        :rtype: ArrayLike
        """
        return self._func(*args, **kwargs)


# see https://en.wikipedia.org/wiki/Time_complexity#Table_of_common_time_complexities
O1 = Complexity("O(1)", lambda _: 1.0)  # constant
On = Complexity("O(n)", lambda n: n)  # linear
Ologn = Complexity("O(log n)", lambda n: np.log2(n))  # logarithmic
Onlogn = Complexity("O(n log n)", lambda n: n * np.log2(n))  # log linear
On2 = Complexity("O(n^2)", lambda n: np.power(n, 2))  # quadratic
On3 = Complexity("O(n^3)", lambda n: np.power(n, 3))  # cubic
Ologlogn = Complexity(
    "O(log log n)", lambda n: np.log2(np.log2(n))
)  # double logarithmic

DEFAULT_COMPLEXITIES: List[Complexity] = [O1, On, Ologn, Onlogn, On2, On3, Ologlogn]
