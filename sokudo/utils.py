import ctypes
import glob
import os
import platform
import subprocess
import sys
import json
import cpuinfo
import numpy as np
import rich
from typing import Callable, Dict, Optional, List, Tuple
from sokudo.config import Config
from sokudo.types import ArrayLike

IS_WINDOWS: bool = sys.platform == "win32"
IS_64BIT: bool = ctypes.sizeof(ctypes.c_void_p) == 8


def _locate_cuda() -> Optional[str]:
    # try to get the path of cuda by looking at env variables
    cuda_home: Optional[str] = os.environ.get("CUDA_HOME") or os.environ.get(
        "CUDA_PATH"
    )

    if cuda_home is None:
        # try to get the path of cuda by locating nvcc (cuda compiler)
        try:
            which: str = "where" if IS_WINDOWS else "which"
            nvcc: str = subprocess.check_output([which, "nvcc"]).decode().rstrip("\r\n")
            cuda_home = os.path.dirname(os.path.dirname(nvcc))
        except subprocess.CalledProcessError:
            # nvcc is not recognised
            if IS_WINDOWS:
                homes: List[str] = glob.glob(
                    "C:/Program Files/NVIDIA GPU Computing Toolkit/CUDA/v*.*"
                )
                if len(homes) == 0:
                    cuda_home = ""
                else:
                    cuda_home = homes[0]
            else:
                cuda_home = "/usr/local/cuda"

            if not os.path.exists(cuda_home):
                cuda_home = None

    return cuda_home


def get_cuda_version() -> Optional[str]:
    """Return installed Cuda version.

    :raises RuntimeError: NVCC is not present in Cuda path.
    :return: Cuda version if exists, None otherwise.
    :rtype: Optional[str]
    """
    cuda_path: Optional[str] = _locate_cuda()

    if cuda_path is None:
        return None

    # check for version.txt
    version_file: str = os.path.join(cuda_path, "version.txt")
    if os.path.isfile(version_file):
        with open(version_file) as f:
            version: str = f.readline().replace("\n", "").replace("\r", "")
            return version.split(" ")[2][:4]

    # check for version.json
    version_file = os.path.join(cuda_path, "version.json")
    if os.path.isfile(version_file):
        with open(version_file) as f:
            version = json.load(f)["cuda"]["version"]
            return version

    # check with nvcc
    try:
        output = subprocess.check_output(
            [os.path.join(cuda_path, "bin", "nvcc"), "--version"]
        )
        version = str(output).replace("\n", "").replace("\r", "")
        # example: Cuda compilation tools, release 11.4, V11.4.100
        idx = version.find("release")
        stop_idx = version[idx + len("release") :].find(",")
        return version[idx + len("release ") : idx + len("release ") + stop_idx]
    except subprocess.CalledProcessError:
        raise RuntimeError("Cannot get cuda version")


# https://github.com/flababah/cpuid.py/blob/master/cpuid.py
class CPUIDStruct(ctypes.Structure):
    _fields_ = [(reg, ctypes.c_uint32) for reg in ("eax", "ebx", "ecx", "edx")]


_POSIX_64_OPC: bytes = (
    b"\x53"  # push   %rbx
    b"\x89\xf0"  # mov    %esi,%eax
    b"\x89\xd1"  # mov    %edx,%ecx
    b"\x0f\xa2"  # cpuid
    b"\x89\x07"  # mov    %eax,(%rdi)
    b"\x89\x5f\x04"  # mov    %ebx,0x4(%rdi)
    b"\x89\x4f\x08"  # mov    %ecx,0x8(%rdi)
    b"\x89\x57\x0c"  # mov    %edx,0xc(%rdi)
    b"\x5b"  # pop    %rbx
    b"\xc3"  # retq
)

_WINDOWS_64_OPC: bytes = (
    b"\x53"  # push   %rbx
    b"\x89\xd0"  # mov    %edx,%eax
    b"\x49\x89\xc9"  # mov    %rcx,%r9
    b"\x44\x89\xc1"  # mov    %r8d,%ecx
    b"\x0f\xa2"  # cpuid
    b"\x41\x89\x01"  # mov    %eax,(%r9)
    b"\x41\x89\x59\x04"  # mov    %ebx,0x4(%r9)
    b"\x41\x89\x49\x08"  # mov    %ecx,0x8(%r9)
    b"\x41\x89\x51\x0c"  # mov    %edx,0xc(%r9)
    b"\x5b"  # pop    %rbx
    b"\xc3"  # retq
)

_CDECL_32_OPC: bytes = (
    b"\x53"  # push   %ebx
    b"\x57"  # push   %edi
    b"\x8b\x7c\x24\x0c"  # mov    \xc(%esp),%edi
    b"\x8b\x44\x24\x10"  # mov    \x10(%esp),%eax
    b"\x8b\x4c\x24\x14"  # mov    \x14(%esp),%ecx
    b"\x0f\xa2"  # cpuid
    b"\x89\x07"  # mov    %eax,(%edi)
    b"\x89\x5f\x04"  # mov    %ebx,\x4(%edi)
    b"\x89\x4f\x08"  # mov    %ecx,\x8(%edi)
    b"\x89\x57\x0c"  # mov    %edx,\xc(%edi)
    b"\x5f"  # pop    %edi
    b"\x5b"  # pop    %ebx
    b"\xc3"  # ret
)


class CPUID:
    def __init__(self):
        """Create a class to call ``cpuid``.

        :raises SystemError: Only x86 is supported.
        :raises MemoryError: Memory allocation failed.
        :raises OSError: The memory protection change failed.
        """
        if platform.machine() not in ("AMD64", "x86_64", "x86", "i686"):
            raise SystemError("Only available for x86")

        if IS_WINDOWS:
            if IS_64BIT:
                opc: bytes = _WINDOWS_64_OPC
            else:
                opc: bytes = _CDECL_32_OPC
        else:
            opc: bytes = _POSIX_64_OPC if IS_64BIT else _CDECL_32_OPC

        size: int = len(opc)
        code = (ctypes.c_ubyte * size).from_buffer(bytearray(opc))

        if IS_WINDOWS:
            self.kernel32 = ctypes.CDLL("kernel32.dll")
            self.kernel32.VirtualAlloc.restype = ctypes.c_void_p
            self.kernel32.VirtualAlloc.argtypes = [
                ctypes.c_void_p,
                ctypes.c_size_t,
                ctypes.c_ulong,
                ctypes.c_ulong,
            ]
            self.addr = self.kernel32.VirtualAlloc(None, size, 0x1000, 0x40)
            if not self.addr:
                raise MemoryError("Could not allocate memory")
        else:
            self.libc = ctypes.cdll.LoadLibrary(None)
            self.libc.valloc.restype = ctypes.c_void_p
            self.libc.valloc.argtypes = [ctypes.c_size_t]
            self.addr = self.libc.valloc(size)
            if not self.addr:
                raise MemoryError("Could not allocate memory")

            self.libc.mprotect.restype = ctypes.c_int
            self.libc.mprotect.argtypes = [
                ctypes.c_void_p,
                ctypes.c_size_t,
                ctypes.c_int,
            ]
            if self.libc.mprotect(self.addr, size, 1 | 2 | 4) != 0:
                raise OSError("Failed to change memory protection")

        ctypes.memmove(self.addr, code, size)

        func_type = ctypes.CFUNCTYPE(
            None, ctypes.POINTER(CPUIDStruct), ctypes.c_uint32, ctypes.c_uint32
        )
        self.func_ptr = func_type(self.addr)

    def __call__(self, eax: int, ecx: int = 0) -> Tuple[int, int, int, int]:
        """Call ``cpuid``.

        :param eax: EAX register
        :type eax: int
        :param ecx: ECX register, defaults to 0
        :type ecx: int, optional
        :return: EAX, EBX, ECX and EDX registers
        :rtype: Tuple[int, int, int, int]
        """
        regs = CPUIDStruct()
        self.func_ptr(regs, eax, ecx)
        return regs.eax, regs.ebx, regs.ecx, regs.edx

    def __del__(self) -> None:
        # free memory
        if IS_WINDOWS:
            self.kernel32.VirtualFree.restype = ctypes.c_long
            self.kernel32.VirtualFree.argtypes = [
                ctypes.c_void_p,
                ctypes.c_size_t,
                ctypes.c_ulong,
            ]
            self.kernel32.VirtualFree(self.addr, 0, 0x8000)
        elif self.libc:
            self.libc.free.restype = None
            self.libc.free.argtypes = [ctypes.c_void_p]
            self.libc.free(self.addr)


def _is_bit_set(reg: int, bit: int) -> bool:
    mask: int = 1 << bit
    return (reg & mask) > 0


def get_cpu_features() -> Dict[str, bool]:
    """Get CPU features (such as SIMD).

    :return: A dict of features.
    :rtype: Dict[str, bool]
    """
    cpuid = CPUID()
    eax, _, _, _ = cpuid(0, 0)

    # https://en.wikipedia.org/wiki/CPUID#EAX=0:_Highest_Function_Parameter_and_Manufacturer_ID
    features: Dict[str, bool] = {}

    if eax >= 1:
        # https://en.wikipedia.org/wiki/CPUID#EAX=1:_Processor_Info_and_Feature_Bits
        _EDX: Dict[str, int] = {"MMX": 23, "SSE": 25, "SSE2": 26}
        _ECX: Dict[str, int] = {
            "SSE3": 0,
            "FMA3": 12,
            "SSE4.1": 19,
            "SSE4.2": 20,
            "AVX": 28,
            "AES": 25,
        }

        eax, ebx, ecx, edx = cpuid(1, 0)
        for ext, bit in _EDX.items():
            features[ext] = _is_bit_set(edx, bit)
        for ext, bit in _ECX.items():
            features[ext] = _is_bit_set(ecx, bit)

    # https://en.wikipedia.org/wiki/CPUID#EAX=7,_ECX=0:_Extended_Features
    if eax >= 7:
        _EBX: Dict[str, int] = {
            "BMI1": 3,
            "AVX2": 5,
            "BMI2": 8,
            "SHA": 29,
            "AVX512F": 16,
            "AVX512CD": 28,
            "AVX512PF": 26,
            "AVX512ER": 27,
            "AVX512VL": 31,
            "AVX512BW": 30,
            "AVX512DQ": 17,
            "AVX512IFMA": 21,
        }
        _ECX = {"AVX512VBMI": 2, "AVX512VBMI2": 6, "VAES": 9}

        eax, ebx, ecx, edx = cpuid(7, 0)
        for ext, bit in _EBX.items():
            features[ext] = _is_bit_set(ebx, bit)
        for ext, bit in _ECX.items():
            features[ext] = _is_bit_set(ecx, bit)
    return features


def get_cpu_name() -> str:
    """Get CPU name.

    :return: CPU name.
    :rtype: str
    """
    return cpuinfo.get_cpu_info()["brand_raw"]


def _vectorize(
    func: Callable[[ArrayLike], ArrayLike]
) -> Callable[[ArrayLike], ArrayLike]:
    trials: np.ndarray = np.array([2.0, 5.0, 6.0])

    try:
        output: np.ndarray = np.array(func(trials))

        if output.shape == trials.shape:
            return func
    except TypeError:
        return np.vectorize(func)

    raise RuntimeError(f"'{func.__name__}' is not an element-wise function.")


def print_header(config: Config) -> None:
    """Print some system informations.

    :param config: Configuration
    :type config: Config
    """
    if config.show_cpu:
        rich.print(f"[bold underline]CPU:[/bold underline] {get_cpu_name()}")

    if config.show_cuda:
        cuda = get_cuda_version()
        if cuda:
            rich.print(f"[bold underline]Cuda:[/bold underline] {cuda}")

    if config.show_cudnn:
        # TODO
        pass

    if config.show_simd:
        features = get_cpu_features()
        enabled_features = [name for (name, enabled) in features.items() if enabled]
        rich.print(
            f"[bold underline]CPU Features:[/bold underline] {','.join(enabled_features)}"
        )
