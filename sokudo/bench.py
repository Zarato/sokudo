import collections
from functools import partial
import gc
import itertools
import time
import numpy as np
import rich
import rich.progress
from dataclasses import dataclass
from typing import Any, Callable, Dict, Iterable, List, Optional, Union
from sokudo.complexity import Complexity, ElementWiseFunc, DEFAULT_COMPLEXITIES
from sokudo.state import State
from sokudo.types import BenchmarkEntry, BenchmarkResult
from sokudo.reporters.reporter import Reporter
from sokudo.utils import print_header
from sokudo.config import config

units: Dict[str, float] = {"ns": 1e-9, "us": 1e-6, "ms": 1e-3, "s": 1.0}
default_timer: Callable[[], Union[int, float]] = time.perf_counter


def _compute_iterations(func: Callable, args: Optional[Dict[str, Any]] = None) -> int:
    i = 1
    while True:
        for j in 1, 2, 5:
            number = i * j
            state = State(number, default_timer)
            if args:
                func(state, **args)
            else:
                func(state)

            total_time: float = float(np.sum(state._times))
            if total_time >= 0.2:
                return number

        i *= 10


class Benchmark:
    """Benchmark functions."""

    def __init__(
        self, title: str, compute_complexity: bool = False, warmup: int = 0
    ) -> None:
        """Create a benchmark.

        :param title: Title of the benchmark.
        :param compute_complexity: Set to ``True`` for computing complexities, defaults to False
        :param warmup: Warming up the functions, defaults to 0
        """

        self._title = title
        self._unit = "ns"
        # self._complexities = DEFAULT_COMPLEXITIES
        # self._compute_complexity = compute_complexity
        self._warmup = warmup
        self._benchmarks: List[BenchmarkEntry] = []
        self._results: List[BenchmarkResult] = []
        self._reporters: List["Reporter"] = []
        self._gccold = False

    @property
    def unit(self) -> str:
        """Returns the unit.

        :return: Unit.
        :rtype: str
        """
        return self._unit

    @unit.setter
    def unit(self, name: str) -> None:
        """Set the unit.

        :param name: Unit.
        :raises ValueError: Unit is not valid.
        """
        if name not in units:
            raise ValueError(f"Unit {name} is not recognised")
        self._unit = name

    """
    @property
    def complexities(self) -> List[Complexity]:
        return self._complexities
    """

    @property
    def warmup(self) -> int:
        """Returns the number of warmups.

        :return: Number of warmups.
        """
        return self._warmup

    @warmup.setter
    def warmup(self, value: int) -> None:
        """Set the number of warmups.

        :param value: Number of warmups
        :raises ValueError: Number of warmups must be greater than 0.
        """
        if value < 0:
            raise ValueError("'value' must be greater than 0.")
        self._warmup = value

    def run(self):
        """Run benchmarks"""
        # print some informations
        print_header(config)

        if self._warmup > 0:
            rich.print(f"[bold underline]Warmup:[/bold underline] {self._warmup}")

        for entry in self._benchmarks:
            progress = rich.progress.Progress(
                rich.progress.SpinnerColumn(),
                *rich.progress.Progress.get_default_columns(),
                "{task.completed}/{task.total}",
            )
            progress.start()

            warmup = False
            if entry.args:
                # make args
                args_names: List[str] = list(entry.args.keys())

                for args in itertools.product(*entry.args.values()):
                    kwargs: Dict[str, Any] = {
                        args_names[i]: arg for i, arg in enumerate(args)
                    }

                    # check if warmup is enabled
                    if not warmup and self._warmup > 0:
                        for _ in range(self._warmup):
                            # create a dummy state
                            entry.func(State(1, default_timer), **kwargs)
                        warmup = True

                    # check if iterations is specified
                    iterations = entry.iterations or _compute_iterations(
                        entry.func, kwargs
                    )

                    # create task for progress bar
                    task = progress.add_task(
                        f"[cyan]{entry.name}{'/' + '/'.join(map(str, args))}",
                        total=iterations * entry.repetitions,
                    )

                    times: np.ndarray = np.zeros((entry.repetitions, iterations))
                    for i in range(entry.repetitions):
                        state = State(iterations, default_timer)
                        self._suspend_gc()
                        entry.func(state, **kwargs)
                        self._restore_gc()
                        times[i, :] = state._times
                        progress.update(task, advance=iterations)

                    # TODO : Complexities
                    result = BenchmarkResult(
                        entry.name,
                        iterations,
                        entry.repetitions,
                        times,
                        args=kwargs,
                    )
                    self._results.append(result)
            else:
                # no args
                # check if warmup is enabled
                if not warmup and self._warmup > 0:
                    for _ in range(self._warmup):
                        # create a dummy state
                        entry.func(State(1, default_timer))
                    warmup = True

                # check if iterations is specified
                iterations = entry.iterations or _compute_iterations(entry.func)

                # create task for progress bar
                task = progress.add_task(
                    f"[cyan]{entry.name}",
                    total=iterations * entry.repetitions,
                )

                times: np.ndarray = np.zeros((entry.repetitions, iterations))
                for i in range(entry.repetitions):
                    state = State(iterations, default_timer)
                    self._suspend_gc()
                    entry.func(state)
                    self._restore_gc()
                    times[i, :] = state._times

                # TODO : Complexities

                result = BenchmarkResult(
                    entry.name,
                    iterations,
                    entry.repetitions,
                    times,
                )
                self._results.append(result)

            progress.stop()

        # do report
        self.report()

    def report(self):
        """Report results."""
        for reporter in self._reporters:
            reporter.report(self._title, self._results, self._unit, units[self._unit])

    def register(
        self,
        func: Optional[Callable] = None,
        name: Optional[str] = None,
        iterations: Optional[int] = None,
        repetitions: Optional[int] = None,
        args: Optional[Dict[str, Any]] = None,
        # complexity_arg: str = "n",
    ) -> Callable:
        """Register a function.

        :param func: Function, defaults to None
        :param name: Name of the function. If ``None``, ``func.__name__`` is used, defaults to None
        :param iterations: Number of iterations. If ``None``, determined automatically, defaults to None
        :param repetitions: Number of repetitions. If ``None``, set to 1, defaults to None
        :param args: Args, defaults to None
        :return: Function.
        """
        if func:
            name = name or func.__name__
            if args:
                # make args iterable so that we can use itertools.product
                args = {
                    k: v if isinstance(v, collections.abc.Iterable) else [v]
                    for k, v in args.items()
                }
            repetitions = repetitions or 1
            self._benchmarks.append(
                BenchmarkEntry(
                    func, name, iterations, repetitions, args  # , complexity_arg
                )
            )
            return func

        return partial(
            self.register,
            name=name,
            iterations=iterations,
            repetitions=repetitions,
            args=args,
        )

    """
    def register_complexity(self, name: str, func: ElementWiseFunc) -> None:
        if not any((name == other.name for other in self._complexities)):
            self._complexities.append(Complexity(name, func))
    """

    def register_reporter(self, reporter: Reporter) -> None:
        """Register a reporter.

        :param reporter: Reporter.
        """
        self._reporters.append(reporter)

    def _suspend_gc(self) -> None:
        if not self._gccold:
            self._gccold = gc.isenabled()
            gc.disable()

    def _restore_gc(self) -> None:
        if self._gccold:
            gc.enable()
