import dataclasses
from typing import Any


@dataclasses.dataclass
class Config:
    show_cpu: bool = True
    show_cuda: bool = True
    show_cudnn: bool = True
    show_simd: bool = True

    def __getitem__(self, key: str) -> Any:
        return super().__getattribute__(key)

    def __setitem__(self, key: str, value: Any) -> None:
        super().__setattr__(key, value)


config = Config()
