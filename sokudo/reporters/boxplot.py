import numpy as np
import matplotlib.pyplot as plt
import matplotlib
import matplotlib.cm
from typing import Any, Dict, List
from sokudo.reporters.reporter import Reporter
from sokudo.types import BenchmarkResult

cmap = matplotlib.cm.get_cmap("Set2")


class BoxplotReporter(Reporter):
    """Report the results in a boxplot."""

    def __init__(self, file: str) -> None:
        """Create a Boxplot Reporter.

        :param file: Output file.
        """
        self._file = file

    def report(
        self, title: str, results: List[BenchmarkResult], unit: str, scale: float
    ) -> bool:
        labels: List[str] = []
        data = np.array([result.times.flatten() / scale for result in results]).T
        norm = matplotlib.colors.Normalize(vmin=0.0, vmax=len(results))

        print(data.shape)
        for result in results:
            name = result.name
            if result.args:
                name += "/" + "/".join(map(str, result.args.values()))
            labels.append(name)

        fig, ax = plt.subplots()
        ax.set_title(title)
        ax.set_ylabel(f"Time ({unit})")
        ax.set_xlabel("Benchmarks")
        ax.grid(True)
        bplot = ax.boxplot(data, labels=labels, sym="", patch_artist=True)

        # fill with colors
        for i, patch in enumerate(bplot["boxes"]):
            patch.set_facecolor(cmap(norm(i)))

        plt.savefig(self._file, dpi=300)
        return True
