import rich
import rich.table
import numpy as np
from typing import List
from sokudo.reporters.reporter import Reporter
from sokudo.types import BenchmarkResult


class ConsoleReporter(Reporter):
    """Reports the results in the console."""

    def __init__(self, alpha: float = 0.95) -> None:
        """Create a Console Reporter.

        :param alpha: Confidence level, defaults to 0.95
        """
        self._alpha = alpha

    def report(
        self, title: str, results: List[BenchmarkResult], unit: str, scale: float
    ) -> bool:
        table = rich.table.Table(
            title=title, show_header=True, header_style="bold magenta"
        )

        table.add_column("Name")
        table.add_column("Iterations")
        table.add_column("Repetitions")
        table.add_column(f"Mean ({unit})")
        table.add_column(f"Std ({unit})")
        table.add_column(f"Median ({unit})")
        table.add_column(f"CI ({self._alpha*100:.2f} %)")
        table.add_column(f"Min ({unit})")
        table.add_column(f"Max ({unit})")

        # metrics
        means = [np.mean(result.times) for result in results]
        stds = [np.std(result.times) for result in results]
        medians = [np.median(result.times) for result in results]
        ci = [
            np.quantile(result.times, (self._alpha / 2, 1 - self._alpha / 2))
            for result in results
        ]
        mins = [np.min(result.times) for result in results]
        maxs = [np.max(result.times) for result in results]

        # best
        best_mean = np.argmin(means)
        best_std = np.argmin(stds)
        best_median = np.argmin(medians)
        best_min = np.argmin(mins)
        best_max = np.argmin(maxs)

        # worst
        worst_mean = np.argmax(means)
        worst_std = np.argmax(stds)
        worst_median = np.argmax(medians)
        worst_min = np.argmax(mins)
        worst_max = np.argmax(maxs)

        # print metrics
        for i, result in enumerate(results):
            name = result.name
            if result.args:
                name += "/" + "/".join(map(str, result.args.values()))

            def format_metric(value, refs):
                result = ""
                if i == refs[0]:
                    result += "[bold green]"
                elif i == refs[1]:
                    result += "[bold red]"
                result += "{value:.3f}".format(value=value / scale)
                return result

            table.add_row(
                name,
                str(result.iterations),
                str(result.repetitions),
                format_metric(means[i], (best_mean, worst_mean)),
                format_metric(stds[i], (best_std, worst_std)),
                format_metric(medians[i], (best_median, worst_median)),
                f"[{ci[i][0]/scale:.3f}, {ci[i][1]/scale:.3f}]",
                format_metric(mins[i], (best_min, worst_min)),
                format_metric(maxs[i], (best_max, worst_max)),
            )

        rich.print(table)
        return True
