import abc
from typing import List
from sokudo.types import BenchmarkResult


class Reporter(abc.ABC):
    @abc.abstractclassmethod
    def report(
        self, title: str, results: List[BenchmarkResult], unit: str, scale: float
    ) -> bool:
        """Report the results.

        :param title: Title of the benchmark.
        :param results: Results.
        :param unit: Unit (e.g. ``s``).
        :param scale: Unit scale (e.g. :math:`10^{-6}` for ``us``).
        :return: ``True`` if no errors.
        """
        ...
