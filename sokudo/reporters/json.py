import numpy as np
import json
from typing import Any, Dict, List
from sokudo.reporters.reporter import Reporter
from sokudo.types import BenchmarkResult


class JsonReporter(Reporter):
    """Report the results as a JSON file."""

    def __init__(self, file: str, alpha: float = 0.95) -> None:
        """Create a JSON reporter.

        :param file: Output file.
        :param alpha: Confidence level, defaults to 0.95
        """
        self._filename = file
        self._alpha = alpha

    def report(
        self, title: str, results: List[BenchmarkResult], unit: str, scale: float
    ) -> bool:
        data: Dict[str, Any] = dict(title=title, unit=unit, scale=scale)
        entries: List[Dict[str, Any]] = []

        for result in results:
            mean = np.mean(result.times) / scale
            std = np.std(result.times) / scale
            median = np.median(result.times) / scale
            ci = (
                np.quantile(result.times, (self._alpha / 2, 1 - self._alpha / 2))
                / scale
            )
            min_time = np.min(result.times) / scale
            max_time = np.max(result.times) / scale

            entry: Dict[str, Any] = dict(
                iterations=result.iterations,
                repetitions=result.repetitions,
                args=result.args,
                mean=mean,
                std=std,
                median=median,
                ci=list(ci),
                min=min_time,
                max=max_time,
            )
            entries.append(entry)

        data["results"] = entries

        with open(self._filename, "w") as f:
            f.write(json.dumps(data))

        return True
