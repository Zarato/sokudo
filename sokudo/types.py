from dataclasses import dataclass
from typing import Any, Callable, Dict, List, Optional, Union
import numpy as np

ArrayLike = Union[np.ndarray, List[float], List[int], float, int]


@dataclass
class BenchmarkEntry:
    func: Callable
    name: str
    iterations: Optional[int] = None
    repetitions: Optional[int] = None
    args: Optional[Dict[str, Any]] = None
    # complexity_arg: str


@dataclass
class BenchmarkResult:
    name: str
    iterations: int
    repetitions: int
    times: np.ndarray
    args: Optional[Dict[str, Any]] = None
