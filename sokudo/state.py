import numpy as np

from typing import Callable, Union


class State:
    def __init__(self, max_iters: int, timer: Callable[[], Union[int, float]]) -> None:
        """Benchmark state.

        :param max_iters: Maximum number of iterations
        :param timer: Function that returns the time.
        """

        assert max_iters > 0, "At least one iteration must be run"
        self._max_iters = max_iters
        self._timer = timer
        self._started = False
        self._finished = False
        self._times = np.zeros(max_iters)
        self._iter = -1
        self._start: Union[int, float] = 0

    def pause(self) -> None:
        """Pause the timer."""

        stop = self._timer()
        if self._started:
            if 0 <= self._iter < self._max_iters:
                self._times[self._iter] += stop - self._start
            self._started = False

    def resume(self) -> None:
        """Resume the timer."""

        assert not self._started, "The benchmark is already running"

        self._started = True
        self._start = self._timer()

    def __next__(self) -> int:
        """Iterate over the state.

        :raises StopIteration: Stop iteration.
        :return: Number of iteration.
        """

        self.pause()
        i = self._iter
        if i >= self._max_iters:
            self._finished = True
            raise StopIteration
        self._iter += 1
        self.resume()
        return i

    def __iter__(self) -> "State":
        """Create an iterable.

        :return: Iterable.
        """
        return self
