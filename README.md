# :zap: Sokudo

<div align="center">
    <img src="assets/code_image.png">
</div>

[[_TOC_]]

## Getting started

### Prerequisites

- Python version >= 3.8

### Installation

TODO

## Usage

See `examples` for more information.

```python
import numpy as np
import math
from sokudo import Benchmark, State
from sokudo.reporters.console import ConsoleReporter

bench = Benchmark("Log")
bench.unit = "ms"
n_repeat = 5
args = {"n": [10**i for i in range(3, 7)]}
rng = np.random.default_rng()


@bench.register(repetitions=n_repeat, args=args)
def math_log(state: State, n: int) -> None:
    x = rng.integers(1, 10**4, size=n)
    y = [0.0] * n

    for _ in state:
        for i in range(n):
            y[i] = math.log(x[i])


@bench.register(repetitions=n_repeat, args=args)
def np_log(state: State, n: int) -> None:
    x = rng.integers(1, 10**4, size=n)

    for _ in state:
        np.log(x)


if __name__ == "__main__":
    bench.register_reporter(ConsoleReporter())
    bench.run()
```

## Screenshots

Example output of `examples/math_log.py`:

[![asciicast](https://asciinema.org/a/rOG2WEAm9o2U2a8CaqVTrhtNe.svg)](https://asciinema.org/a/rOG2WEAm9o2U2a8CaqVTrhtNe)

## Contributing

Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.

## License

[MIT](https://choosealicense.com/licenses/mit/)
