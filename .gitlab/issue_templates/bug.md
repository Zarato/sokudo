### Summary
<!-- Summarise the bug encountered. -->

#### Expected result
<!-- Describe what you should see. -->

#### Actual result
<!-- Describe what actually happens. -->

### Reproduction steps
<!-- Describe how one can reproduce the issue. -->

### Environment
<!-- Please provide your development environment here -->
- **Operating System**: Windows/Linux
- **Architecture**: x86/ARM64/PowerPC ...
- **Python version**: 3.8
- **Sokudo version**: 0.1.0

### Additional information

/label ~bug