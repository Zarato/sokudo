sokudo.reporters package
========================

.. currentmodule:: sokudo.reporters

.. automodule:: sokudo.reporters

Implements various :class:`Reporter` for reporting results of benchmarks.

.. autosummary::
   :toctree: _autosummary

    Reporter
    ConsoleReporter
    JsonReporter
    BoxplotReporter