Sokudo reference documentation
==================================

.. toctree::
   :maxdepth: 2
   :hidden:
   :caption: Getting Started

   installation
   usage
   examples

.. toctree::
   :maxdepth: 2
   :hidden:
   :caption: Reference documentation

   sokudo

.. toctree::
   :maxdepth: 2
   :hidden:
   :caption: About

   license

.. hint::
   View source code of `sokudo on Gitlab <https://gitlab.com/Zarato/sokudo>`_.

``sokudo`` is a microbenchmark Python package for versions ``>=3.8``.

.. code-block:: python3

   import numpy as np
   from sokudo import Benchmark, State
   from sokudo.reporters import ConsoleReporter, JsonReporter, BoxplotReporter

   bench = Benchmark("Norm", warmup=1)
   bench.unit = "us"
   n_iterations = 10_000
   n_repeat = 7
   args = {"n": 10**5}
   rng = np.random.default_rng()


   @bench.register(iterations=n_iterations, repetitions=n_repeat, args=args)
   def np_norm(state: State, n: int) -> None:
      x = rng.normal(size=n)

      for _ in state:
         np.linalg.norm(x)


   @bench.register(iterations=n_iterations, repetitions=n_repeat, args=args)
   def np_sum_sq(state: State, n: int) -> None:
      x = rng.normal(size=n)

      for _ in state:
         np.sqrt(np.sum(x**2))


   if __name__ == "__main__":
      bench.register_reporter(ConsoleReporter())
      bench.register_reporter(JsonReporter("output.json"))
      bench.register_reporter(BoxplotReporter("output.jpg"))
      bench.run()

This scripts prints the following

=================== ============= ============== ============ =========== ============== =================== =========== ===========
  Name                Iterations    Repetitions    Mean (us)    Std (us)    Median (us)    CI (95.00 %)        Min (us)    Max (us)       
=================== ============= ============== ============ =========== ============== =================== =========== ===========
  np_norm/100000      10000         7              24.189       122.063     13.790         [13.758, 13.823]    12.137      6513.312       
  np_sum_sq/100000    10000         7              61.899       41.863      59.958         [59.936, 59.979]    56.526      6268.051                                                                                                                   
=================== ============= ============== ============ =========== ============== =================== =========== ===========

A JSON file ``output.json`` and the following boxplot

.. image:: boxplot.jpg
   :alt: Example of output

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
