Usage
=====

#. Create a simple benchmark code, in ``bench_mean``:

   .. code-block:: python3

      import numpy as np
      from sokudo import Benchmark, State
      from sokudo.reporters import ConsoleReporter

      bench = Benchmark("Mean", warmup=1)
      bench.unit = "us"
      n_iterations = 10_000
      n_repeat = 7
      args = {"n": 10**5}
      rng = np.random.default_rng()

      @bench.register(iterations=n_iterations, repetitions=n_repeat, args=args)
      def np_norm(state: State, n: int) -> None:
          x = rng.normal(size=n)

          for _ in state:
              np.linalg.mean(x)

      if __name__ == "__main__":
          bench.register_reporter(ConsoleReporter())
          bench.run()

#. Run the benchmark with

   .. code:: sh

      python bench_mean.py
