.. currentmodule:: sokudo

Reference documentation
=======================

Subpackages
-----------

.. toctree::
   :maxdepth: 1

   sokudo.reporters

.. _sokudo:

Base objects
------------

.. autosummary::
   :toctree: _autosummary

    Benchmark
    State
