import time
import numpy as np
from sokudo import State


def test_state_iter():
    max_iters: int = 5
    state = State(max_iters, time.perf_counter)

    duration: float = 0.2  # 200ms
    for _ in state:
        time.sleep(duration)

    assert np.all(state._times >= duration)
    assert np.all(state._times < duration * 1.2)
    assert state._iter == max_iters


def test_state_pause():
    max_iters: int = 5
    state = State(max_iters, time.perf_counter)

    duration: float = 0.2  # 200ms
    for _ in state:
        state.pause()
        time.sleep(duration)
        state.resume()
        time.sleep(duration)

    assert np.all(state._times >= duration)
    assert np.all(state._times < duration * 1.22)
    assert state._iter == max_iters
