import math
import subprocess
import os
import numpy as np
import pytest
from typing import Dict, Optional
from sokudo.utils import get_cuda_version, get_cpu_name, get_cpu_features, _vectorize


def test_get_cuda_version():
    cuda_version: Optional[str] = get_cuda_version()

    try:
        output = subprocess.check_output(["nvcc", "--version"])
        # if no errors occurs, check that ``get_cuda_version`` has a return value
        assert cuda_version is not None
    except subprocess.CalledProcessError:
        # TODO : write a better test
        # cuda is maybe not available
        assert cuda_version is None
    except FileNotFoundError:
        assert cuda_version is None


def test_get_cpu_name():
    cpu: str = get_cpu_name()
    assert "GHz" in cpu


def test_get_cpu_features():
    features: Dict[str, bool] = get_cpu_features()

    # AVX and SSE are available on most computers
    assert features["AVX"] == True
    assert features["SSE"] == True


def test_vectorize():
    def f(x: float) -> float:
        return math.log(x)

    def g(x: np.ndarray) -> float:
        return np.sum(x)

    x: np.ndarray = np.array([5, 6, 8, 9])
    y: np.ndarray = np.log(x)

    f_vect = _vectorize(f)

    assert np.all(f_vect(x) == y)

    # assert that ``RuntimeError`` is raised for g
    with pytest.raises(RuntimeError):
        _vectorize(g)
