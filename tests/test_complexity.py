import numpy as np
from sokudo.complexity import Ologn


def test_complexity_logn():
    rng: np.random.Generator = np.random.default_rng()
    size: int = 100
    x: np.ndarray = rng.integers(1, 10**4, size=size)
    y: np.ndarray = np.log2(x)

    assert np.all(Ologn(x) == y)
